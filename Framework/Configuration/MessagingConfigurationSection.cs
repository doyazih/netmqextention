﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Framework.Configuration
{
	class MessagingConfigurationSection : ConfigurationSection
	{
		public const string SECTION_PATH = "ConnApi.configuration/messaging";

		private static readonly ConfigurationProperty _ConnectionStringsProp
			= new ConfigurationProperty(
				"connmqs",
				typeof(ConnMqsElementCollection),
				null);

		[ConfigurationProperty("connmqs", IsRequired = false)]
		public ConnMqsElementCollection ConnMqs
		{
			get
			{
				ConnMqsElementCollection connMqCollection = (ConnMqsElementCollection)base[_ConnectionStringsProp];
				return connMqCollection;
			}
		}


		public static MessagingConfigurationSection Current
		{
			get
			{
				return ConfigurationManager.GetSection(MessagingConfigurationSection.SECTION_PATH)
					as MessagingConfigurationSection;
			}
		}

	}

	[ConfigurationCollection(typeof(ConnMqElementCollection), AddItemName = "connmq")]
	public class ConnMqsElementCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new ConnMqElementCollection();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((ConnMqElementCollection)element).Name;
		}

		public new ConnMqElementCollection this[string Name]
		{
			get
			{
				return (ConnMqElementCollection)BaseGet(Name);
			}
		}

	}

	[ConfigurationCollection(typeof(ConnMqSettingElement))]
	public class ConnMqElementCollection : ConfigurationElementCollection
	{
		protected override ConfigurationElement CreateNewElement()
		{
			return new ConnMqSettingElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return (((ConnMqSettingElement)element).Host + ":" + ((ConnMqSettingElement)element).Port);
		}

		public ConnMqSettingElement this[int idx]
		{
			get
			{
				return (ConnMqSettingElement)BaseGet(idx);
			}
		}

		[ConfigurationProperty("name", IsRequired = true)]
		public string Name
		{
			get
			{
				return (string)this["name"];
			}
			set
			{
				this["name"] = value;
			}
		}

		[ConfigurationProperty("type", IsRequired = true)]
		public string Type
		{
			get
			{
				return (string)this["type"];
			}
			set
			{
				this["type"] = value;
			}
		}
	}

	public class ConnMqSettingElement : ConfigurationElement
	{
		[ConfigurationProperty("protocol")]
		public string Protocol
		{
			get
			{
				return (string)this["protocol"];
			}
			set
			{
				this["protocol"] = value;
			}
		}

		[ConfigurationProperty("host")]
		public string Host
		{
			get
			{
				return (string)this["host"];
			}
			set
			{
				this["host"] = value;
			}
		}

		[ConfigurationProperty("port")]
		public int Port
		{
			get
			{
				return (int)this["port"];
			}
			set
			{
				this["port"] = value;
			}
		}
	}
}
