﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMQ;
using System.Threading;

namespace Framework.Messaging.ZeroMQ
{
	public delegate void PullDelegate(string data);

	public class ZeroMqMessagingPullManager : ZeroMqMessagingManager
	{	
		private PullDelegate _pullDelegate;
		private bool _isRunnable;

		public ZeroMqMessagingPullManager(string configSectionName, PullDelegate pullDelegate)
			: base(configSectionName, SocketType.Client)
		{
			_pullDelegate = pullDelegate;
		}

		protected override NetMQSocket CreateSocketType(ref NetMQContext context)
		{
			return context.CreatePullSocket();
		}

		protected override void CreateSocket(ref NetMQSocket socket)
		{
			base.CreateSocket(ref socket);

			if(Socket != null)
				_isRunnable = true;
		}

		public virtual string Receive()
		{
			string result = string.Empty;

			try
			{
				if (Socket != null)
				{
					lock (Socket)
					{
						if (Socket != null)
							result = Socket.ReceiveString();
					}
				}
			}
			catch (NetMQException ne)
			{
#if DEBUG
				System.Diagnostics.Trace.WriteLine("Pull receive fail exception");
#endif
			}

			return result;
		}

		public virtual void Run()
		{
			while (_isRunnable)
			{
				string data = Receive();

				_pullDelegate(data);
			}
		}

		public virtual void Start()
		{
			Thread thred = new Thread(Run);

			thred.Start();
		}

		public override void Dispose()
		{
			_isRunnable = false;

			if (Socket != null)
			{
				foreach (string conn in ConnectionAddresses)
				{
					Socket.Disconnect(conn);
				}
				Socket.Dispose();
				Socket.Close();
				Socket = null;
			}

			base.Dispose();
		}
	}
}
