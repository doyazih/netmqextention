﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMQ;
using System.Threading;

namespace Framework.Messaging.ZeroMQ
{
	public delegate void SubDelegate(string data);

	public class ZeroMqMessagingSubManager : ZeroMqMessagingManager
	{
		private SubDelegate _subDelegate;
		private bool _isRunnable;

		public ZeroMqMessagingSubManager(string configSectionName, SubDelegate subDelegate)
			: base(configSectionName, SocketType.Client)
		{
			_subDelegate = subDelegate;
		}

		protected override NetMQSocket CreateSocketType(ref NetMQContext context)
		{
			return context.CreateSubscriberSocket();
		}

		protected override void CreateSocket(ref NetMQSocket socket)
		{
			base.CreateSocket(ref socket);

			if (Socket != null)
			{
				socket.Subscribe(string.Empty);

				_isRunnable = true;
			}			
		}

		public virtual string Receive()
		{
			string result = string.Empty;

			try
			{
				if(Socket != null)
				{
					lock(Socket)
					{
						if(Socket != null)
							result = Socket.ReceiveString();
					}
				}
			}
			catch (NetMQException ne)
			{
#if DEBUG
				System.Diagnostics.Trace.WriteLine("Subscribe receive fail exception");
#endif
			}

			return result;
		}

		public virtual void Run()
		{
			while(_isRunnable)
			{
				string data = Receive();

				_subDelegate(data);
			}
		}

		public virtual void Start()
		{
			Thread thred = new Thread(Run);

			thred.Start();
		}

		public override void Dispose()
		{
			_isRunnable = false;

			if (Socket != null)
			{
				foreach (string conn in ConnectionAddresses)
				{
					Socket.Disconnect(conn);
				}
				Socket.Close();
				Socket.Dispose();
				Socket = null;
			}

			base.Dispose();
		}
	}
}
