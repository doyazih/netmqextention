﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMQ;
using System.Net;
using System.Text.RegularExpressions;
using Framework.Configuration;

namespace Framework.Messaging.ZeroMQ
{
	public abstract class ZeroMqMessagingManager : IMessagingManager
	{
		#region 숨김속성
		private List<ZeroMqDeviceConfig> _configs;
		private NetMQContext _context;
		private NetMQSocket _socket;
		private SocketType _socketType;

		private string _bindingAddress;
		private List<string> _connectionAddresses;
		#endregion

		#region abstract
		/// <summary>
		/// NetMQ Socket Type setting
		/// </summary>
		//protected abstract NetMQSocket CreateSocket();
		protected abstract NetMQSocket CreateSocketType(ref NetMQContext context);
		//public abstract void Dispose();
		#endregion

		#region Properties
		public List<ZeroMqDeviceConfig> Configs
		{
			get
			{
				return _configs;
			}
			set
			{
				_configs = value;
			}
		}

		public NetMQContext Context
		{
			get
			{
				return _context;
			}
		}

		public NetMQSocket Socket
		{
			get
			{
				return _socket;
			}
			set
			{
				_socket = value;
			}
		}

		public string BindingAddress
		{
			get
			{
				return _bindingAddress;
			}
		}

		public List<string> ConnectionAddresses
		{
			get
			{
				return _connectionAddresses;
			}
		}

		public SocketType SocketType
		{
			get
			{
				return _socketType;
			}
		}
		#endregion

		#region Constructor

		public ZeroMqMessagingManager(string configSectionName, SocketType socketType)
		{
			this._socketType = socketType;
			SetConfiguration(configSectionName);
		}
		#endregion

		#region virtual

		protected virtual void CreateContext()
		{
			if (_context == null)
			{
				lock (typeof(NetMQContext))
				{
					if (_context == null)
					{
						_context = NetMQContext.Create();
					}
				}
			}
		}

		protected virtual void CreateSocket(ref NetMQSocket socket)
		{
			if (socket == null)
			{
				lock (typeof(NetMQSocket))
				{
					if (this.SocketType.Equals(SocketType.Server))
					{
						SetBindingAddress();

						if (!string.IsNullOrEmpty(BindingAddress))
						{
							CreateContext();
							socket = CreateSocketType(ref _context);

							socket.Bind(BindingAddress);

							this._socket = socket;
						}
					}
					else if (this.SocketType.Equals(SocketType.Client))
					{
						SetConnectionAddresses();

						if (ConnectionAddresses != null && ConnectionAddresses.Count > 0)
						{
							CreateContext();
							socket = CreateSocketType(ref _context);

							foreach (string addr in ConnectionAddresses)
							{
								socket.Connect(addr);
							}

							this._socket = socket;
						}
					}
				}
			}
		}

		public virtual void SetBindingAddress()
		{
			if (Configs != null && Configs.Count > 0 && Configs[0].Port != 0 && Configs[0].ProtocolType.GetType().Equals(typeof(ZeroMqProtocolType)))
			{
				ZeroMqDeviceConfig config = Configs[0];

				if (string.IsNullOrEmpty(config.Address))
					config.Address = Dns.GetHostAddresses(Dns.GetHostName()).Where(w => w.AddressFamily.ToString().Equals("InterNetwork", StringComparison.CurrentCultureIgnoreCase)).First().ToString();

				_bindingAddress = GetAddress(config);
			}
		}

		protected virtual void SetConnectionAddresses()
		{
			var ipPattern = "((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";	//IP 체크 regular exp

			if (Configs != null && Configs.Count > 0)
			{
				_connectionAddresses = new List<string>();

				Configs.ForEach(c =>
				{

					//Ip, port, protocol type 체크 후 connection list에 추가
					if ((Regex.IsMatch(c.Address, ipPattern) || c.Address.Equals("localhost", StringComparison.InvariantCultureIgnoreCase)) && c.Port != 0 && c.ProtocolType.GetType().Equals(typeof(ZeroMqProtocolType)))
						_connectionAddresses.Add(GetAddress(c));
				});
			}
		}

		public virtual void Dispose()
		{
			if (_context != null)
			{
				_context.Dispose();
				_context.Terminate();
			}
		}

		/// <summary>
		/// Binding 용 socket 시 single address
		/// </summary>
		protected virtual void Initialize(ZeroMqDeviceConfig config)
		{
			Initialize(new List<ZeroMqDeviceConfig>{
				config
			});
		}

		/// <summary>
		/// Connection address mutli 필요시
		/// </summary>
		protected virtual void Initialize(List<ZeroMqDeviceConfig> configs)
		{
			_configs = configs;

			SetupDevice();
		}

		protected virtual void SetupDevice()
		{
			if (_configs.Count <= 0)
			{
				throw new Exception("ZeroMQ config is not set");
			}
		}

		#endregion

		#region functions
		protected List<string> GetAddresses()
		{
			return _configs.Select(s => GetAddress(s)).ToList<string>();
		}

		protected string GetAddress(ZeroMqDeviceConfig config)
		{
			string result;

			switch (config.ProtocolType)
			{
				case ZeroMqProtocolType.TCP:
					result = string.Format("tcp://{0}:{1}", config.Address, config.Port.ToString());
					break;
				case ZeroMqProtocolType.INPROC:
					result = string.Format("inproc://{0}", config.Address);
					break;
				case ZeroMqProtocolType.IPC:
					result = string.Format("ipc:///tmp/filename");
					break;
				case ZeroMqProtocolType.PGM:
					result = string.Format("pgm://interface;address:port");
					break;
				default:
					result = string.Empty;
					break;
			}

			return result;
		}

		public void SetConfiguration(string configSectionName)
		{
			if (MessagingConfigurationSection.Current != null && MessagingConfigurationSection.Current.ConnMqs.Count > 0)
			{
				ConnMqElementCollection zeroMqConfigs = MessagingConfigurationSection.Current.ConnMqs[configSectionName];

				if (zeroMqConfigs != null)
				{
					//config 및 type이 setting 되었는지 체크
					if (zeroMqConfigs.Count > 0 && Enum.Parse(typeof(ConnApiMessagingType), zeroMqConfigs.Type, true).Equals(ConnApiMessagingType.ZeroMQ))
					{
						_configs = new List<ZeroMqDeviceConfig>();

						foreach (ConnMqSettingElement zeroMqConfig in zeroMqConfigs)
						{
							_configs.Add(new ZeroMqDeviceConfig
							{
								Address = zeroMqConfig.Host,
								Port = zeroMqConfig.Port,
								ProtocolType = (ZeroMqProtocolType)Enum.Parse(typeof(ZeroMqProtocolType), zeroMqConfig.Protocol, true)
							});
						}
					}
				}
			}
		}
		#endregion
	}

	#region Server/Client (Socket bind/connection) type
	public enum SocketType
	{
		Server,
		Client
	}
	#endregion
}
