﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMQ;
using Newtonsoft.Json;

namespace Framework.Messaging.ZeroMQ
{
	public class ZeroMqMessagingPubManager : ZeroMqMessagingManager
	{
		public ZeroMqMessagingPubManager(string configSectionName)
			: base(configSectionName, SocketType.Server)
		{
		}	

		protected override NetMQSocket CreateSocketType(ref NetMQContext context)
		{
			return context.CreatePublisherSocket();
		}

		public virtual void Send(string data)
		{
			try
			{
				if(Socket != null)
				{
					lock(Socket)
					{	
						if(Socket != null)
							Socket.Send(data, true);
					}
				}
			}
			catch (AgainException ae)
			{
				//push 실패 시 코드 구현
#if DEBUG
				System.Diagnostics.Trace.WriteLine("Publish fail Exception catch : \n" + data);
#endif
			}
			catch (Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry("ConnMqMessagingService", string.Format("Send fail exception\nException : {0}\nData : {1}", ex.Message, data));
			}
		}

		public virtual void Send(ConnApiMessagingT data)
		{
			this.Send(JsonConvert.SerializeObject(data));
		}

		public override void Dispose()
		{
			if (Socket != null)
			{
				Socket.Unbind(BindingAddress);
				Socket.Dispose();
				Socket.Close();
				Socket = null;
			}

			base.Dispose();
		}
	}
}
