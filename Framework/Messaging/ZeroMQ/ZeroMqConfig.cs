﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.Messaging.ZeroMQ
{
	public class ZeroMqDeviceConfig
	{
		public ZeroMqProtocolType ProtocolType { get; set; }
		public string Address { get; set; }
		public int Port { get; set; }
	}

	public enum ZeroMqProtocolType
	{
		TCP, INPROC, IPC, PGM
	}
}
