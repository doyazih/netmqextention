﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.Messaging
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public class ConnApiMessagingAttribute : Attribute
	{
		public ConnApiMessagingAttribute(ConnApiMessagingType msgType, string methodName)
		{
		}

		public ConnApiMessagingAttribute(ConnApiMessagingType msgType, string methodName, string name)
		{
		}
	}
}
