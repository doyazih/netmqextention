﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.Messaging
{
	public class ConnApiMessagingT
	{
		public string MethodName { get; set; }
		public Type ClassType { get; set; }
		public string SpName { get; set; }
		public List<ConnApiMessagingParamT> Parameters { get; set; }
		public ConnApiMessagingConfigInfo MessagingConfig { get; set; }
	}

	public class ConnApiMessagingParamT
	{
		public Type ParameterType { get; set; }
		public object Value { get; set; }
	}

	public class ConnApiMessagingConfigInfo
	{
		public ConnApiMessagingType MessagingType { get; set; }
		public string MessagingModuleName { get; set; }
	}
}
