﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetMQ;
using System.Threading;


namespace ZeroMqConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            PushMq pm = new PushMq();

            Thread thread = new Thread(pm.Run);

            thread.Start();

            Thread.Sleep(1000);
            Console.ReadLine();
        }
    }
}
