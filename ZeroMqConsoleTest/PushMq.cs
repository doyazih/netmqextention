﻿using NetMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZeroMqConsoleTest
{
    public class PushMq : PushMqBase
    {
        private static bool IsOn = false;
        public static int gidx = 0;

        public static NetMQContext context;
        public static NetMQSocket socket;

        public PushMq() :base(ref context, ref socket)
        {
        }

        public void Run()
        {
            while(IsOn)
            {
                try
                {
                    socket.Send(gidx + ":" + DateTime.Now.ToString());
                }
                catch(AgainException ae)
                {
                    System.Diagnostics.Trace.WriteLine(ae.Message);
                }
            }
        }

        public static void Switch()
        {
            if (IsOn)
                IsOn = false;
            else
                IsOn = true;
        }
    }
}
