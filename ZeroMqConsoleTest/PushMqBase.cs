﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NetMQ;

namespace ZeroMqConsoleTest
{
    public class PushMqBase : MqContext
    {
        public PushMqBase(ref NetMQContext context, ref NetMQSocket socket)
        { 
            if(context != null)
            {
                lock (context)
                {
                    context = NetMQContext.Create();
                    socket = context.CreatePushSocket();

                    socket.Bind("tcp://10.0.2.15:5550");
                }
            }
        }
    }
}
